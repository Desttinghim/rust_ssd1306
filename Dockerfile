# Start from alpine edge for chip base...
FROM xtacocorex/chiptainer_alpine_edge

# Install needed packages
RUN apk update && \
    apk add linux-headers && \
    apk add git && \
    apk add gcc && \
    apk add make 

# Compile
RUN git clone https://github.com/vkomenda/ssd1306 && \
    cd ssd1306 && \
    make && \
    cd ../

# Cleanup
RUN

# Run program on starting the container
ENTRYPOINT ./ssd1306/ssd1306
