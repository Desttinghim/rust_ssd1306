#!/bin/sh

CONTAINER_IMAGE=${CONTAINER_IMAGE:-chiptainer_ssd1306}

case "$1" in
	build)
		docker build --no-cache=true -t "${CONTAINER_IMAGE}" .
		;;
	tag)
		docker tag chiptainer_ssd1306 desttinghim/chiptainer_ssd1306
		;;
	push)
		docker push desttinghim/chiptainer_ssd1306
		;;
	all)
		echo "BUILDING"
		docker build --no-cache=true -t "${CONTAINER_IMAGE}" .
		echo "TAGGING"
		docker tag chiptainer_ssd1306 desttinghim/chiptainer_ssd1306
		echo "PUSHING"
		docker push desttinghim/chiptainer_ssd1306
		;;
	remove-tags)
		docker rmi `docker images | grep chiptainer_ssd1306 | grep "<none>" | tr -s " " | cut -d " " -f 3`
		;;
esac
